//
//  MessageService.swift
//  Smack
//
//  Created by cameron nigro on 9/14/17.
//  Copyright © 2017 Sheepware. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MessageService {
	static let instance = MessageService()
	
	var channels = [Channel]()
	
	func findAllChannels(completion: @escaping CompletionHandler) {
		Alamofire.request(URL_GET_CHANNELS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BEARER_HEADER).responseJSON { (response) in
			if response.result.error == nil {
				guard let data = response.data else { return }

				do {
					self.channels = try JSONDecoder().decode([Channel].self, from: data)
				} catch let error {
					debugPrint(error as Any)
				}
				print(self.channels)
			} else {
				completion(false)
				debugPrint(response.result.error as Any)
			}
		}
	}
}
