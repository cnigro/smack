//
//  CreateAccountVC.swift
//  Smack
//
//  Created by cameron nigro on 9/2/17.
//  Copyright © 2017 Sheepware. All rights reserved.
//

import UIKit

class CreateAccountVC: UIViewController {

	// Outlets
	@IBOutlet weak var userNameText: UITextField!
	@IBOutlet weak var emailText: UITextField!
	@IBOutlet weak var passwordText: UITextField!
	@IBOutlet weak var userImage: UIImageView!
	@IBOutlet weak var spinner: UIActivityIndicatorView!
	
	// Variables
	var avatarName = "profileDefault"
	var avatarColor = "[0.5, 0.5, 0.5, 1]"
	var bgColor: UIColor?
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		setupView()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		if UserDataService.instance.avatarName != "" {
			userImage.image = UIImage(named: UserDataService.instance.avatarName)
			avatarName = UserDataService.instance.avatarName
			if avatarName.contains("light") && bgColor == nil {
				userImage.backgroundColor = UIColor.lightGray
			}
		}
	}

	@IBAction func closePressed(_ sender: Any) {
		performSegue(withIdentifier: UNWIND, sender: nil)
	}
	
	func setupView() {
		spinner.isHidden = true
		
		userNameText.attributedPlaceholder = NSAttributedString(string: "username",
			attributes: [NSAttributedStringKey.foregroundColor: smackPurplePlaceHolder])
		emailText.attributedPlaceholder = NSAttributedString(string: "email",
			attributes: [NSAttributedStringKey.foregroundColor: smackPurplePlaceHolder])
		passwordText.attributedPlaceholder = NSAttributedString(string: "password",
			attributes: [NSAttributedStringKey.foregroundColor: smackPurplePlaceHolder])
		
		let tap = UITapGestureRecognizer(target: self, action: #selector(CreateAccountVC.handleTap))
		view.addGestureRecognizer(tap)
	}
	
	@objc func handleTap() {
		view.endEditing(true)
	}
	
	@IBAction func createAcountPressed(_ sender: Any) {
		spinner.isHidden = false
		spinner.startAnimating()
		
		guard let name = userNameText.text , userNameText.text != "" else { return }
		guard let email = emailText.text , emailText.text != "" else { return }
		guard let pass = passwordText.text , passwordText.text != "" else { return }
		AuthService.instance.registerUser(email: email, password: pass) { (success) in
			if success {
				AuthService.instance.loginUser(email: email, password: pass, completion: { (success) in
					if success {
						AuthService.instance.createUser(name: name,
						                                email: email,
						                                avatarName: self.avatarName,
						                                avatarColor: self.avatarColor,
						                                completion: { (success) in
							if success {
								self.spinner.isHidden = true
								self.spinner.stopAnimating()
								self.performSegue(withIdentifier: UNWIND, sender: nil)
								NotificationCenter.default.post(name: NOTIF_USER_DATA_DID_CHANGE, object: nil)
							}
						})
					}
				})
			}
		}
	}
	
	@IBAction func pickAvatarPressed(_ sender: Any) {
		performSegue(withIdentifier: TO_AVATAR_PICKER, sender: nil)
	}
	
	@IBAction func pickBGColorPressed(_ sender: Any) {
		let r = CGFloat(arc4random_uniform(255)) / 255
		let g = CGFloat(arc4random_uniform(255)) / 255
		let b = CGFloat(arc4random_uniform(255)) / 255
		
		bgColor = UIColor(red: r, green: g, blue: b, alpha: 1)
		avatarColor = "[\(r), \(g), \(b), 1]"
		UIView.animate(withDuration: 0.2) {
			self.userImage.backgroundColor = self.bgColor
		}
	}
	
}
