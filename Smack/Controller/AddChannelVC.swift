//
//  AddChannelVCViewController.swift
//  Smack
//
//  Created by cameron nigro on 9/24/17.
//  Copyright © 2017 Sheepware. All rights reserved.
//

import UIKit

class AddChannelVC: UIViewController {
	
	// Outlets
	@IBOutlet weak var nameText: UITextField!
	@IBOutlet weak var chanDesc: UITextField!
	@IBOutlet weak var bgView: UIView!
	

    override func viewDidLoad() {
        super.viewDidLoad()
		setupView()
    }
	
	@IBAction func closeModalPressed(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	
	@IBAction func createChannelPressed(_ sender: Any) {
		
	}
	
	func setupView() {
		let closeTouch = UITapGestureRecognizer(target: self, action: #selector(AddChannelVC.closeTap(_:)))
		bgView.addGestureRecognizer(closeTouch)
		nameText.attributedPlaceholder =
			NSAttributedString(string: "name",attributes: [NSAttributedStringKey.foregroundColor: smackPurplePlaceHolder])
		chanDesc.attributedPlaceholder =
			NSAttributedString(string: "description",attributes: [NSAttributedStringKey.foregroundColor: smackPurplePlaceHolder])
	}
	
	@objc func closeTap(_ recognizer: UITapGestureRecognizer) {
		dismiss(animated: true, completion: nil)
	}
	
}
