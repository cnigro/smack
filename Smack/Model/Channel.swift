//
//  Channel.swift
//  Smack
//
//  Created by cameron nigro on 9/14/17.
//  Copyright © 2017 Sheepware. All rights reserved.
//

import Foundation

struct Channel: Decodable {
	public private(set) var _id: String!
	public private(set) var name: String!
	public private(set) var description: String!
	public private(set) var __v: Int?
}
