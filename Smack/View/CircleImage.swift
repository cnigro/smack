//
//  CircleImage.swift
//  Smack
//
//  Created by cameron nigro on 9/6/17.
//  Copyright © 2017 Sheepware. All rights reserved.
//

import UIKit

class CircleImage: UIImageView {

	override func awakeFromNib() {
		setupView()
	}
	
	func setupView() {
		self.layer.cornerRadius = self.frame.width / 2
		self.clipsToBounds = true
	}
	
	override func prepareForInterfaceBuilder() {
		super.prepareForInterfaceBuilder()
		setupView()
	}

}
